// console.log("Hello World");

// [SECTION] Array Methods


// Mutator Methods

let fruits =["Apple", "Orange", "Kiwi", "Dragon Fruits"];

// push()

console.log("Console array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements/value to an array
fruits.push("avocado", "guava");

console.log("Mutated array from push method:");
console.log(fruits)

// pop()

//Removes the last element in a n array AND returns the removed element
//Syntax
//arrayName.pop();

let removedFruit =fruits.pop;
console.log(removedFruit);
console.log("Mutated array from push method:");
console.log(fruits)

fruits.pop()
console.log("Mutated array from pop method:");
console.log(fruits)

// Unshift()


fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits)


// Shift()


let anotherFruit = fruits.shift();
console.log(anotherFruit)
console.log("Mutated array from shift method:");
console.log(fruits)

// Splice()

// 

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits)

// Sort()

// Rearranges the array elements in alphanumeric order


fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits)

// Reverse()
// arrayName.reverse()

fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits)


// Non-Mutator Methods

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE" ]

//indexOf()
//Syntax --> arrayName.indexOf(SearchValue);
//Syntax --> arrayName.indexOf(SearchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry)

//lastIndexOf()
// Syntax --> arrayName.lastIndexOf(searchValue)

// getting the index number starting from the last element
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method " + lastIndex);

//getting the Index number starting from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method " + lastIndexStart);


// Slice

let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);


let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()

//Syntax
// arrayName.toString(); 

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

// concat()


let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method");
console.log(tasks);

//Combining multiple arrays:
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);


//Combined arrays with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result from concat method");
console.log(combinedTasks);

//join()

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' ! '));


//Iteration Methods

//forEach()
//similar to for loops
//syntax
//arrayName.forEach(function(indivElement)){statement}

allTasks.forEach(function(task){
	console.log(task);
});

// Using forEach with conditional statements

let filteredTasks =[];

//Looping through all Array Items

allTasks.forEach(function(task){
// if the element/string's length is greater than 10 characters
if(task.length > 10){
	filteredTasks.push(task);
}
});
console.log("Result of filtered tasks:");
console.log(filteredTasks);

//map()
//syntax let/const resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;

});
console.log("Original Array:");
console.log(numbers); //
console.log("Result of map method");
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach);


// every()
//
//Syntax --> let/const resultArray = arrayName.every(function(){

// })

let allValid = numbers.every(function(number){
	return(number > 0);
});

console.log("Result of every method");
console.log(allValid);

// some()

let someValid = numbers.some(function(number){
	return (number < 1);
});
console.log("Result from some method:");
console.log(someValid);

if (someValid){
	console.log("Some numbers in the array are greater that are freater that 2");
};


//filter()
//Syntax   --> let/const resultArrayName.filter(function(indivElement){
//	return expression/condition
// });

let filterValid = numbers.filter(function(number){
	return (number < 4);
});

console.log("Result from filter method");
console.log(filterValid);


// No element found

let nothingFound = numbers.filter(function(number){
return (number = 0);


});
console.log("Result from nothingFound method");
console.log(nothingFound);


//filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});
console.log("Result from filter method");
console.log(filteredNumbers);

// includes()
// Syntax --> arrayName.includes(<arguments>)

let products = ["Mouse", "Keyboard", "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

//reduce()
//Syntax --> let/const result = arrayName.reduce(function(accumulator, currentValue){
// 	return expression/condition
// });

let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){
	console.warn("current iteration: " + ++iteration)
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);


	return x + y;
});

console.log("Result of reduce method " + reduceArray); 